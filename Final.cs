using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proyecto_final
{
    class cajero
    {
        private double monto;
        private int Cant_Cuotas;
        private double interesAnual;
        private double interesMensual;
        private double interesCuota;
        private double Cuotas;
        private double Capital;
        private double Balance;
        DateTime fecha = new DateTime();

        public void Ingresar()
        {
            Console.Write("Monto: " );
            monto = double.Parse(Console.ReadLine());

            Console.Write("Cantidad de cuotas: ");
            Cant_Cuotas = int.Parse(Console.ReadLine());

            Console.Write("Interes anual: ");
            interesAnual = double.Parse(Console.ReadLine());

            fecha = DateTime.Today;
        }

        public void TablaAmortizacion()
        {
            double[] arrayCuota = new double[Cant_Cuotas];
            double[] arrayCapital = new double[Cant_Cuotas];
            double[] arrayInteresCuota = new double[Cant_Cuotas];
            double[] arrayBalance = new double[Cant_Cuotas];
            DateTime[] arrayFecha = new DateTime[Cant_Cuotas];
            

            interesMensual = (interesAnual / 100) / 12;
            Cuotas = (interesMensual * monto)/(1-(Math.Pow((1 + interesMensual), -Cant_Cuotas)));
            Balance = monto;

            interesCuota = Balance * interesMensual;
            Capital = Cuotas - interesCuota;

            Console.WriteLine("Valor cuota; "+Cuotas);

            for (int i = 1; i <= Cant_Cuotas; i++)
            {
                Balance = Balance - Capital;
                
                arrayCuota[i-1] = Cuotas;
                arrayCapital[i - 1] = Capital;
                arrayInteresCuota[i - 1] = interesCuota;
                arrayBalance[i - 1] = Balance;

                interesCuota = Balance * interesMensual;
                Capital = Cuotas - interesCuota;

                fecha = fecha.AddMonths(1);
                arrayFecha[i - 1] = fecha;

            }

            arrayBalance[Cant_Cuotas - 1] = 0;

            for (int i = 0; i < Cant_Cuotas; i++)
            {
                Console.WriteLine("----------------------------");
                Console.WriteLine("Pago: " + (i+1));
                Console.WriteLine("Fecha de pago: " + arrayFecha[i].ToShortDateString());
                Console.WriteLine("Cuota: " + arrayCuota[i]);
                Console.WriteLine("Capital: " + arrayCapital[i]);
                Console.WriteLine("Interes: " + arrayInteresCuota[i]);
                Console.WriteLine("Balance: " + arrayBalance[i]);
                Console.WriteLine("----------------------------");
            }
        }

    }
    class Program
    {
        static void Main(string[] args)
        {
            cajero cliente = new cajero();
            short rep = 0;

            while (rep == 0)
            {
                try
                {
                    cliente.Ingresar();
                    cliente.TablaAmortizacion();
                }
                catch
                {
                    Console.WriteLine("Ha ocurrido un error");
                }
                Console.WriteLine("Pulse 0 para repetir de lo contrario pulse cualquier otro numero");
                rep=short.Parse(Console.ReadLine());
                Console.Clear();
            }
            Console.ReadKey();
        }
    }
}
