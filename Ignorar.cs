using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proyecto_final
{
    class cajero
    {
        private double monto;
        private int Cant_Cuotas;
        private double interesAnual;
        private double Cuotas;
        private double Capital;
        private double Balance;
        DateTime fecha = new DateTime();

        public void Ingresar()
        {
            Console.Write("Monto: " );
            monto = double.Parse(Console.ReadLine());

            Console.Write("Cantidad de cuotas: ");
            Cant_Cuotas = int.Parse(Console.ReadLine());

            Console.Write("Interes anual: ");
            interesAnual = double.Parse(Console.ReadLine());

            fecha = DateTime.Today;
        }

        public void TablaAmortizacion()
        {
            double interesMensual = (interesAnual / 100) / 12;
            Cuotas = (interesMensual * monto)/(1-(Math.Pow((1 + interesMensual), -Cant_Cuotas)));
            Balance = monto;

            double interesCuota = Balance * interesMensual;
            Capital = Cuotas - interesCuota;

            Console.WriteLine("Valor cuota; "+Cuotas);

            for (int i = 1; i <= Cant_Cuotas; i++)
            {
                Balance = Balance - Capital;

                Console.WriteLine("----------------------------");
                Console.WriteLine("Pago: "+i);
                Console.WriteLine("Fecha de pago: "+ fecha.ToShortDateString());
                Console.WriteLine("Cuota: "+Cuotas);
                Console.WriteLine("Capital: "+Capital);
                Console.WriteLine("Interes: "+interesCuota);
                Console.WriteLine("Balance: "+Balance);
                Console.WriteLine("----------------------------");

                interesCuota = Balance * interesMensual;
                Capital = Cuotas - interesCuota;

                fecha = fecha.AddMonths(1);
            }
        }

    }
    class Program
    {
        static void Main(string[] args)
        {
            cajero cliente = new cajero();
            short rep = 0;

            while (rep == 0)
            {
                try
                {
                    cliente.Ingresar();
                    cliente.TablaAmortizacion();
                }
                catch
                {
                    Console.WriteLine("Ha ocurrido un error");
                }
                Console.WriteLine("Pulse 0 para repetir de lo contrario pulse cualquier otro numero");
                rep=short.Parse(Console.ReadLine());
                Console.Clear();
            }
            Console.ReadKey();
        }
    }
}
